$(document).ready(function() {
    $('.navbar .dropdown').hover(function() {
        $(this).find('.dropdown-menu').first().stop(true, true).delay(150).slideDown();
    }, function() {
        $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp()
    });
    $(window).scroll(function() {

        var e = $(this).scrollTop();
        if (e > 60) {
            $("#scroll-Img").attr('src', 'assets/images/logo-scroll.svg');
        } else if (e < 60) {
            $("#scroll-Img").attr('src', 'assets/images/wexos-logo.svg');
        }
    });
    /* $('.navbar .dropdown').hover(function() {
         $(this).find('.dropdown-menu').first().stop(true, true).delay(150).slideDown();
     }, function() {
         $(this).find('.dropdown-menu').first().stop(true, true).delay(100).slideUp()
     });*/

    // full page scroll

    $('#fullpage').fullpage({
        verticalCentered: true,
        scrollBar: true,
        scrollingSpeed: 700,
        loopTop: false,
        keyboardScrolling: false,
        // navigation: true,
        // navigationPosition: 'right',
        animateAnchor: true
    });
    if (window.innerWidth < 1000) {
        $.fn.fullpage.setResponsive(true);
    }

    var $container = $('.grid').isotope({
        itemSelector: '.element-item',
        isFitWidth: true
    });
    $container.isotope({ filter: '*' });
    $('.filters').on('click', 'li', function() {
        var filterValue = $(this).attr('data-filter');
        $container.isotope({ filter: filterValue });
    });


});
var swiper_solution = new Swiper('.mainslider.swiper-container', {
    direction: 'vertical',
    // slidesPerView: 1,
    // spaceBetween: 30,
    mousewheel: false,
    simulateTouch: false,
    pagination: {
        el: '.swiper-pagination',
        clickable: false,
    },
});
var swiper = new Swiper('.testimonials.swiper-container', {
    slidesPerView: 3,
    spaceBetween: 30,
    autoplay: {
        delay: 3000,
        disableOnInteraction: false,
    },
    pagination: {
        el: '.swiper-pagination',
        clickable: true,
    },
    breakpoints: {
        768: {
            slidesPerView: 1,
            spaceBetween: 10,
        },
        640: {
            slidesPerView: 1,
            spaceBetween: 20,
        },
        320: {
            slidesPerView: 1,
            spaceBetween: 10,
        }

    }
});
var swiper2 = new Swiper('#home-swiper .swiper-container', {
      spaceBetween: 30,
      slidesPerView:1,
      fadeEffect: { crossFade: true },
      // virtualTranslate: true, 
      // speed: 1000, 
      effect: 'fade',
      autoplay: {
        delay: 3000,
        disableOnInteraction: false,
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });

if ($('#map')) {
    // google map
    google.maps.event.addDomListener(window, 'load', init);

    function init() {
        var mapOptions = {
            zoom: 16,
            center: new google.maps.LatLng(12.916236, 77.650654), // New York
            styles: [{ "featureType": "all", "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#000000" }, { "lightness": 40 }] }, { "featureType": "all", "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#000000" }, { "lightness": 16 }] }, { "featureType": "all", "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#000000" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#000000" }, { "lightness": 17 }, { "weight": 1.2 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 20 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 21 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#000000" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#000000" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 16 }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 19 }] }, { "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#000000" }, { "lightness": 17 }] }]
        };
        var mapElement = document.getElementById('map');
        var map = new google.maps.Map(mapElement, mapOptions);
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(12.916236, 77.650654),
            map: map,
            title: 'Snazzy!'
        });
    }
}


// try for free

